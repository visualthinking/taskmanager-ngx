import {AfterContentInit, Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-tasks',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit,AfterContentInit {

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }
  ngAfterContentInit() {

    setTimeout(() => {
      this.spinner.hide('loader');
    }, 1500);
  }

}
