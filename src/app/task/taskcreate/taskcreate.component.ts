import {AfterContentInit, Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-taskcreate',
  templateUrl: './taskcreate.component.html',
  styleUrls: ['./taskcreate.component.scss']
})
export class TaskcreateComponent implements OnInit,AfterContentInit {

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }
  ngAfterContentInit() {

    setTimeout(() => {
      this.spinner.hide('loader');
    }, 1500);
  }
}
