import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    NbButtonModule,
    NbCardModule,
    NbContextMenuModule,
    NbIconModule,
    NbLayoutModule,
    NbListModule,
    NbMenuModule,
    NbSelectModule,
    NbSidebarModule,
    NbThemeModule,
    NbTooltipModule
} from '@nebular/theme';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NB_AUTH_TOKEN_INTERCEPTOR_FILTER, NbAuthJWTInterceptor, NbAuthJWTToken, NbAuthModule, NbPasswordAuthStrategy} from '@nebular/auth';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuardService} from './services/auth-guard.service';
import {SideNavComponent} from './side-nav/side-nav.component';
import {LogoutComponent} from './logout/logout.component';
import {LoginComponent} from './login/login.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {environment} from '../environments/environment';
import {TaskComponent} from './task/task.component';
import { TaskcreateComponent } from './task/taskcreate/taskcreate.component';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        SideNavComponent,
        LogoutComponent,
        LoginComponent,
        TaskComponent,
        TaskcreateComponent,


    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NbThemeModule.forRoot({name: 'corporate'}),
        NbLayoutModule,
        NbMenuModule.forRoot(),
        NbSidebarModule.forRoot(),
        NbEvaIconsModule,
        NbAuthModule.forRoot({
            strategies: [
                NbPasswordAuthStrategy.setup({
                    name: 'email',
                    baseEndpoint: environment.baseUrl,
                    token: {
                        class: NbAuthJWTToken,
                        key: 'token'
                    },
                    login: {
                        redirect: {
                            success: '/dashboard',
                            failure: null, // stay on the same page
                        },
                        endpoint: '/auth/login',
                        method: 'post',
                    },

                }),
            ],
            forms: {
                login: {
                    rememberMe: false,
                },
            },
        }),
        NbButtonModule,
        NbContextMenuModule,
        NbIconModule,
        NbCardModule,
        NgxSpinnerModule,
        NbListModule,
        NbTooltipModule,
        NbSelectModule,
    ],
    providers: [AuthGuardService,
        {provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER, useValue: () => false,},
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NbAuthJWTInterceptor,
            multi: true
        }],
    bootstrap: [AppComponent]
})
export class AppModule {
}
