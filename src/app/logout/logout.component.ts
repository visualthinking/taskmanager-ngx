import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthControllerService} from '../services/auth-controller.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit, AfterViewInit {

  constructor(private spinner: NgxSpinnerService, private auth: AuthControllerService) {

  }

  ngOnInit() {
    this.spinner.show('loader');
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.auth.logout();
    }, 1500);
  }
}
