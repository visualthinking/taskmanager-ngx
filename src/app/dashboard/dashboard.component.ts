import {AfterContentInit, Component} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {DashboardService} from '../services/dashboard.service';
import {interval, Subscription} from 'rxjs';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterContentInit {
    opentasks: any = [];
    pendingtasks: any = [];
    latetasks: any = [];
    subscription: Subscription;
    source = interval(60000);

    constructor(private spinner: NgxSpinnerService, private dasboardService: DashboardService) {

        this.subscription = this.source.subscribe(() => {
            this.getData();
        });

    }

    ngAfterContentInit() {
        this.getData();
        setTimeout(() => {
            this.spinner.hide('loader');
        }, 1500);
    }


    getDate(limitDate: any) {
        return new Date(limitDate).toLocaleString();
    }
    getData(){
        this.dasboardService.getLate().subscribe((response: any) => {
            this.latetasks = response.docs;
        });
        this.dasboardService.getOpen().subscribe((response: any) => {
            this.opentasks = response.docs;
        });
        this.dasboardService.getPending().subscribe((response: any) => {
            this.pendingtasks = response.docs;
        });
    }
}
