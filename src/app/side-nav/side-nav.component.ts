import {Component, OnInit} from '@angular/core';
import {NbSidebarService, NbThemeService} from '@nebular/theme';
import {AuthControllerService} from '../services/auth-controller.service';
import {NbAuthJWTToken, NbAuthService} from '@nebular/auth';
import {interval, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';


@Component({
    selector: 'app-side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
    items = [
        {
            title: 'Dashboard',
            icon: 'monitor-outline',
            link: 'dashboard'
        },
        {

            title: 'User',
            expanded: true,
            icon: 'person',
            children: [
                {
                    title: 'Profile',
                    url: '#', // goes directly into `href` attribute
                    icon: 'edit', // goes directly into `href` attribute
                },
                {
                    title: 'Change Password',
                    link: [], // goes into angular `routerLink`
                    icon: 'lock', // goes into angular `routerLink`
                },
                {
                    title: 'Logout',
                    link: ['/auth/logout'],
                    icon: 'log-out'
                },
                {
                    title: 'Privacy Policy',
                    url: '#', // goes directly into `href` attribute
                    icon: 'book', // goes directly into `href` attribute
                },
            ],
        },
        {
            title: 'Task',
            children: [
                {
                    title: 'View Tasks',
                    link: ['/task'], // goes into angular `routerLink`
                },
                {
                    title: 'Create Task',
                    link: ['/task/create'], // goes directly into `href` attribute
                }
            ],
        },
        {
            title: 'Orders',
            children: [
                {
                    title: 'First Order',
                    link: [], // goes into angular `routerLink`
                },
                {
                    title: 'Second Order',
                    url: '#', // goes directly into `href` attribute
                },
                {
                    title: 'Third Order',
                    link: [],
                },
            ],
        },
    ];
    user = {};
    subscription: Subscription;
    source = interval(500);
    state = false;
    imageSource = 'logo-dk';
    selectedItem: any = 'corporate';
    sidestate: any = 'expanded';
    sidestateBool: any = true;
    containerHide: any = false;

    constructor(private sidebarService: NbSidebarService,
                public auth: AuthControllerService,
                private authService: NbAuthService,
                private router: Router,
                private spinner: NgxSpinnerService,
                private themeService: NbThemeService) {

        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
            this.spinner.show('loader').then();
            this.updateUserInfo(token);
        });
        this.subscription = this.source.subscribe(() => {
            this.state = this.auth.isLoggedIn();
        });

    }

    ngOnInit() {
        if (window.innerWidth <= 1366) {
            console.log(window.innerWidth <= 1366);
            this.sidestate = 'collapsed';
            this.sidestateBool = false;
        }
    }

    toggleSideNavBar() {
        this.sidebarService.toggle(false, 'left');
        this.sidestateBool = !this.sidestateBool;
        if (this.sidestateBool) {
            this.sidestate = 'expanded';
            if (window.innerWidth < 617) {
                this.containerHide = true;
            }
        } else {
            this.sidestate = 'collapsed';
            this.containerHide = false;
        }
        console.log(this.sidestate);
    }

    updateUserInfo(token: NbAuthJWTToken) {
        if (token.isValid()) {
            this.user = token.getPayload();
        }
    }

    updateLogo($event) {
        this.themeService.changeTheme($event);
        switch ($event) {
            case 'corporate':
                this.imageSource = 'logo-dk';
                break;
            case 'cosmic':
                this.imageSource = 'logo';
                break;
            default:
                break;
        }
    }
}
