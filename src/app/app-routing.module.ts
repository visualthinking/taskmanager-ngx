import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NbAuthComponent, NbLoginComponent, NbRequestPasswordComponent, NbResetPasswordComponent} from '@nebular/auth';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuardService} from './services/auth-guard.service';
import {LogoutComponent} from './logout/logout.component';
import {LoginComponent} from './login/login.component';
import {TaskComponent} from './task/task.component';
import {TaskcreateComponent} from './task/taskcreate/taskcreate.component';


const routes: Routes = [{
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
}, {
    path: 'auth',
    component: NbAuthComponent,
    children: [
        {
            path: '',
            component: NbLoginComponent,
        },
        {
            path: 'login',
            component: LoginComponent,
        },
        {
            path: 'logout',
            component: LogoutComponent,
        },
        {
            path: 'request-password',
            component: NbRequestPasswordComponent,
        },
        {
            path: 'reset-password',
            component: NbResetPasswordComponent,
        },
    ],
}, {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuardService]
},
    {
        path: 'task',
        canActivate: [AuthGuardService],
        children: [{
            path: '',
            component: TaskComponent,
        },{
            path: 'create',
            component: TaskcreateComponent,
        }]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
