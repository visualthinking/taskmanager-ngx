import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) {

  }

  getLate() {
    return this.http.get(environment.baseUrl + '/requests/me/late');
  }
  getOpen() {
    return this.http.get(environment.baseUrl + '/requests/me/open');
  }
  getPending() {
    return this.http.get(environment.baseUrl + '/requests/me/pending');
  }
}
