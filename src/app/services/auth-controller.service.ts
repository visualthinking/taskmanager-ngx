import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NbAuthJWTInterceptor, NbAuthJWTToken, NbAuthService, NbTokenService} from '@nebular/auth';

@Injectable({
    providedIn: 'root'
})
export class AuthControllerService {
    constructor(private router: Router,
                private tokenService: NbTokenService, private authService: NbAuthService) {

    }

    logout() {
        this.tokenService.clear();
        this.router.navigate(['auth/login']).then();
    }
    isValidSession(): any {
        let state = true;
        this.authService.getToken().subscribe((token: NbAuthJWTToken) => {
            if (!token.isValid()) {
                state = false;
                this.logout();
            }
            return state;
        });
    }

    isLoggedIn(): any {
        let loginState = false;
        this.authService.isAuthenticated().subscribe((state) => {
            loginState = state;
        });
        return loginState;
    }
}
