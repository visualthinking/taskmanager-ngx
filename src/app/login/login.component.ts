import {Component, OnInit} from '@angular/core';
import {NbLoginComponent} from '@nebular/auth';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends NbLoginComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService) {
    // @ts-ignore
    super();

  }

  ngOnInit(): void {
    setTimeout(() => {
      this.spinner.hide('loader');
    }, 1500);
  }


}
